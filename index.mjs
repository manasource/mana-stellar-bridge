import WebSocket from 'ws';
import Express from 'express';
import bodyParser from 'body-parser';
import albedoSignatureVerification from '@albedo-link/signature-verification'
import * as dotenv from 'dotenv'
dotenv.config()

/**
 * This is for communication with manaserv.
 *
 * Once somebody has signed their token with their Stellar account, we need to
 * send a message to manaserv with the token and the public Stellar key.
 * manaserv can use the public key to identify the account.
 *
 * @type {WebSocket}
 */
let manaserv;

function connectToManaserv() {
  console.log('connecting to manaserv');

  // Open WebSocket connection to manaserv, using the host and port configured
  // in environment variables.
  manaserv = new WebSocket(`ws://${process.env.MANASERV_WS_HOST || "localhost"}:${process.env.MANASERV_WS_PORT || 9605}/stellar-auth`);
  manaserv.on('error', console.error);
  manaserv.on('open', function open() {
    console.log('connected to manaserv');
  });
  manaserv.on('close', function clear() {
    console.log('disconnected from manaserv');
  });
}

connectToManaserv();
setInterval(function ping() {
  if (manaserv.readyState === WebSocket.OPEN) {
    manaserv.ping();
  } else {
    connectToManaserv();
  }
}, 5000);

/**
 * This is for receiving the login results from the browser.
 */
const app = Express();

// Automatically parse the JSON in the body of a POST request.
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

// Allow CORS requests from the configured origin.
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", process.env.STELLAR_BRIDGE_CORS_ORIGIN || "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//
// When we receive the Stellar public key, we still need to verify it, before
// we pass the verified login to manaserv.
//
const {verifyMessageSignature} = albedoSignatureVerification;
app.post('/stellarAuth', function(req, res) {
  const isValid = verifyMessageSignature(
    req.body.pubkey,
    req.body.signed_message,
    req.body.signature
  );

  if (!isValid) {
    res.send('invalid signature');
  } else if (manaserv.readyState !== WebSocket.OPEN) {
    res.send('manaserv not connected');
  } else {
    manaserv.send(JSON.stringify({
      pubkey: req.body.pubkey,
      token: req.body.token
    }));
    res.send('ok');
  }
});

const server = app.listen(process.env.STELLAR_BRIDGE_PORT || 8081);

console.log(`Stellar Bridge listening at ${server.address().address}:${server.address().port}`);
